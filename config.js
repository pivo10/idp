// ##############################################
// IMPORTANT: Adapt the values within the hashtag to productive values!

// url for default config file
exports.urlDefaultConfig = 'www.default-config.de';
// checksum for default config file
exports.checksumDefaultConfig = '1235ABCDEF';

// new_obd_firmware_version_id. This is a bit tricky here. So this ID is the default firmware version that
// newly created devices will install. So this number here needs to exist as primary key in obd_firmware_version
// and it needs to be actual version, that should be installed by default.
exports.defaultFirmwareVersionId = 7;

// the maximum amount of recfiles that can be uploaded with one request
exports.maxAmountRecfilesPerRequest = 10;

// the folder where the validated recfiles are moved to
const recfileQueueFolder = {
    prod: `${__dirname}/recfileQueue`,
    dev: `${__dirname}/recfileQueue`,
    test: `${__dirname}/test/testRecfileQueue`,
};

exports.recfileQueueFolder = recfileQueueFolder[process.env.NODE_ENV || 'dev'];
exports.recfileErrorQueueFolder = `${__dirname}/recfileErrorQueue`;

// ##############################################


// webserver config
const webServerPort = '3000';
const webServerBase = '/api';

exports.webserver = {
    port: webServerPort,
    baseUrl: webServerBase,
};

// database config
const databases = {
    prod: {
        // format: 'dialect://user:password@host/database'
        url: 'postgres://*******************',
        dialect: 'postgres',
        // to avoid 'sequelize deprecated notification' (this is not an actual issue and will be dealed with in sequelize v5)
        operatorsAliases: false,
        logging: false,
        params: {
            schema: {
                device: 'device',
                vehicle: 'vehicle',
            },
        },
    },

    dev: {
        // format: 'dialect://user:password@host/database'
        url: 'postgres://postgres:@localhost/idp',
        dialect: 'postgres',
        // to avoid 'sequelize deprecated notification' (this is not an actual issue and will be dealed with in sequelize v5)
        operatorsAliases: false,
        params: {
            schema: {
                device: 'device',
                vehicle: 'vehicle',
            },
        },
    },

    test: {
        // format: 'dialect://user:password@host/database'
        url: 'postgres://postgres:@localhost/idp',
        dialect: 'postgres',
        logging: false,
        // to avoid 'sequelize deprecated notification' (this is not an actual issue and will be dealed with in sequelize v5)
        operatorsAliases: false,
        params: {
            schema: {
                device: 'device',
                vehicle: 'vehicle',
            },
        },
    },
};

exports.database = databases[process.env.NODE_ENV || 'dev'];

// log file config
const logsPath = './logs/';
const logFile = 'all-logs.log';

exports.logfile = {
    path: logsPath,
    fileName: logFile,
};
