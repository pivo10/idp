/* eslint-disable import/order */

// my app
const app = require('../../app');

// testing dependencies
const request = require('supertest')(app);
const should = require('chai').should();

// mocks
const recfileMock = `${__dirname}/../mocks/recfile.mock.txt`;
const { recfilesBodyMock } = require('../mocks/recfiles.mock');
const { vehicleDeviceTokenMock } = require('../mocks/recfiles.mock');

/**
 * Testing /recfiles endpoint
 */
describe('POST /api/recfiles', () => {
    let recfileBody;
    let vehicleDeviceToken;

    // set recfileBody to valid input before each request
    beforeEach(() => {
        // valid recfileBody input
        recfileBody = recfilesBodyMock;

        // valid user_vehicle_token
        vehicleDeviceToken = vehicleDeviceTokenMock;
    });

    // no header set
    it('respond with 422 because of missing vehicle_device_token in header', (done) => {
        request
            .post('/api/recfiles')
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(422)
            .expect(res => res.body.should.have.property('message', '<No vehicle_device_token or not parseable to string>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    // no recfileMock
    it('respond with 422 because of missing recfile', (done) => {
        request
            .post('/api/recfiles')
            // send authentication token in header
            .set({ vehicle_device_token: vehicleDeviceToken, Accept: 'application/json' })
            .send(recfileBody)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(422)
            .expect(res => res.body.should.have.property('message', '<NoRecfiles>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    // checksum of recfileMock mock does not match the provided sha
    it('respond with 422 because recfile and checksum do not match', (done) => {
        request
            .post('/api/recfiles')
            // send authentication token in header
            .set({ vehicle_device_token: vehicleDeviceToken, Accept: 'application/json' })
            // set multi form data with file
            .field('device_id', recfileBody.device_id)
            .field('user_vehicle_id', recfileBody.user_vehicle_id)
            .field('sha', recfileBody.sha)
            .field('longitude', recfileBody.longitude)
            .field('latitude', recfileBody.latitude)
            // attach recfileMock mock
            .attach('recfile', recfileMock)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(422)
            .expect(res => res.body.should.have.property('message', '<RecfileWrongChecksum>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    // invalid auth token
    it('respond with 401 Unauthorized', (done) => {
        // set valid checksum
        recfileBody.sha = '2851258221accb2cb6d2b48ad6d4b1739084eaa1';
        // set invalid authentication token
        vehicleDeviceToken = 'invalidToken';

        request
            .post('/api/recfiles')
            // send authentication token in header
            .set({ vehicle_device_token: vehicleDeviceToken, Accept: 'application/json' })
            // set multi form data with file
            .field('device_id', recfileBody.device_id)
            .field('user_vehicle_id', recfileBody.user_vehicle_id)
            .field('sha', recfileBody.sha)
            .field('longitude', recfileBody.longitude)
            .field('latitude', recfileBody.latitude)
            // attach recfileMock mock
            .attach('recfile', recfileMock)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(401)
            .expect(res => res.body.should.have.property('message', '<AuthenticationFailed>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });

    it('respond with OK 200', (done) => {
        // set valid checksum
        recfileBody.sha = '2851258221accb2cb6d2b48ad6d4b1739084eaa1';

        request
            .post('/api/recfiles')
            // send authentication token in header
            .set({ vehicle_device_token: vehicleDeviceToken, Accept: 'application/json' })
            // set multi form data with file
            .field('device_id', recfileBody.device_id)
            .field('user_vehicle_id', recfileBody.user_vehicle_id)
            .field('sha', recfileBody.sha)
            .field('longitude', recfileBody.longitude)
            .field('latitude', recfileBody.latitude)
            // attach recfileMock mock
            .attach('recfile', recfileMock)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .expect(res => res.body.should.have.property('message', '<SuccessfulRecfileUpload>'))
            .end((err) => {
                if (err) return done(err);
                return done();
            });
    });
});
