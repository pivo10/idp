// module dependencies
const express = require('express');
const { checkSchema } = require('express-validator/check');

// custom dependencies
const heartbeatLogic = require('../logic/heartbeat/heartbeatLogic');
const Val = require('../logic/Validations');

// router
const router = express.Router();

// send heartbeat to the server
router.post('/', checkSchema(Val.validatePostHeartbeat()), Val.handleValidationErrors,
    Val.checkIfDeviceExists, heartbeatLogic.postHeartbeat);

module.exports = router;
