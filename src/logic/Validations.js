// module dependencies
const { validationResult } = require('express-validator/check');
const _ = require('lodash');
const sha1 = require('sha1');
const fs = require('fs');
const uuidv1 = require('uuid/v1');

// custom dependencies
const Responder = require('./Responder');
const config = require('../../config');
const logger = require('./../../logger');
const DbHandler = require('./DbHandler');

/**
 * Module to centrally handle all input validations.
 */
module.exports = class Validations {
    /**
     * Handle the validation result and fail requests that comes up with validation errors.
     *
     * @param {object} req
     * @param {object} res
     * @param {object} next
     */
    static handleValidationErrors(req, res, next) {
        // finds the validation errors in this request and wraps them in an object
        const errors = validationResult(req);

        // check if errors occurred
        if (!errors.isEmpty()) {
            const responder = new Responder();
            // if validation fails, always log the first occurring error
            const firstError = errors.array()[0];

            // return error response
            return responder.falseAndSend(res, { msgCode: firstError.msg, err: `${firstError.location}: ${firstError.msg}` }, 422);
        }

        // if validation was successful
        return next();
    }

    // ######## VALDATION SCHEMATA ########
    /**
     * Returns a schema to validate the input of a POST /heartbeats request.
     */
    static validatePostHeartbeat() {
        return {
            device_id: {
                in: ['body'],
                errorMessage: '<No device_id or not parseable to string>',
                isString: true,
                escape: true,
                exists: true,
            },
            time: {
                in: ['body'],
                errorMessage: '<No time or not parseable to string>',
                isString: true,
                escape: true,
                exists: true,
            },
            time_previous_wifi_lost: {
                in: ['body'],
                errorMessage: '<No time_previous_wifi_lost or not parseable to string>',
                isString: true,
                escape: true,
                exists: true,
            },
            time_max_rssi: {
                in: ['body'],
                errorMessage: '<No time_max_rssi or not parseable to string>',
                isString: true,
                escape: true,
                exists: true,
            },
            // optional params
            ssid_current_wifi: {
                in: ['body'],
                isString: true,
                errorMessage: '<lat_current_wifi_con not parseable to string>',
                optional: true,
                escape: true,
            },
            lat_current_wifi_con: {
                in: ['body'],
                optional: true,
                errorMessage: '<lat_current_wifi_con not parseable to numeric>',
                isNumeric: true,
                escape: true,
            },
            long_current_wifi_con: {
                in: ['body'],
                optional: true,
                errorMessage: '<long_current_wifi_con not parseable to numeric>',
                isNumeric: true,
                escape: true,
            },
            ssid_previous_wifi: {
                in: ['body'],
                isString: true,
                optional: true,
                errorMessage: '<ssid_previous_wifi not parseable to string>',
                escape: true,
            },
            rssi_previous_wifi_lost: {
                in: ['body'],
                optional: true,
                errorMessage: '<rssi_previous_wifi_lost not parseable to numeric>',
                isNumeric: true,
                escape: true,
            },
            lat_previous_wifi_lost: {
                in: ['body'],
                optional: true,
                errorMessage: '<lat_previous_wifi_lost not parseable to numeric>',
                isNumeric: true,
                escape: true,
            },
            long_previous_wifi_lost: {
                in: ['body'],
                optional: true,
                errorMessage: '<long_previous_wifi_lost not parseable to numeric>',
                isNumeric: true,
                escape: true,
            },
            max_rssi: {
                in: ['body'],
                optional: true,
                errorMessage: '<max_rssi not parseable to numeric>',
                isNumeric: true,
                escape: true,
            },
            lat_max_rssi: {
                in: ['body'],
                optional: true,
                errorMessage: '<lat_max_rssi not parseable to numeric>',
                isNumeric: true,
                escape: true,
            },
            long_max_rssi: {
                in: ['body'],
                optional: true,
                errorMessage: '<long_max_rssi not parseable to numeric>',
                isNumeric: true,
                escape: true,
            },
            free_disk_space: {
                in: ['body'],
                optional: true,
                errorMessage: '<free_disk_space not parseable to numeric>',
                isNumeric: true,
                escape: true,
            },
            number_of_local_files: {
                in: ['body'],
                optional: true,
                errorMessage: '<number_of_local_files not parseable to integer>',
                isInt: true,
                escape: true,
            },
        };
    }

    /**
     * Returns a schema to validate the input of a POST /recfiles request.
     */
    static validatePostRecfile() {
        return {
            // check if all fields that must not be null exist
            vehicle_device_token: {
                in: ['headers'],
                errorMessage: '<No vehicle_device_token or not parseable to string>',
                isString: true,
                exists: true,
                escape: true,
            },
            user_vehicle_id: {
                in: ['body'],
                errorMessage: '<No user_vehicle_id or not parseable to integer>',
                isInt: true,
                exists: true,
                escape: true,
            },
            device_id: {
                in: ['body'],
                errorMessage: '<No device_id or not parseable to string>',
                isString: true,
                exists: true,
                escape: true,
            },
            // at least one checksum has to be provided
            // NOTE: Here, it cannot be tested if files exist since files
            // are part of req.files which is not accessable here
            sha: {
                in: ['body'],
                errorMessage: '<No sha or not parseable to string>',
                isString: true,
                exists: true,
                escape: true,
            },
            // optional params
            longitude: {
                in: ['body'],
                errorMessage: '<longitude not parseable to numeric>',
                isNumeric: true,
                optional: true,
                escape: true,
            },
            latitude: {
                in: ['body'],
                errorMessage: '<latitude not parseable to numeric>',
                isNumeric: true,
                optional: true,
                escape: true,
            },
            source: {
                in: ['body'],
                errorMessage: '<source not parseable to string>',
                isString: true,
                optional: true,
                escape: true,
            },
        };
    }

    /**
     * Returns a schema to validate the input of a GET /updates/check request.
     */
    static validateCheckForUpdates() {
        return {
            device_id: {
                in: ['query'],
                errorMessage: '<No device_id or not parseable to string>',
                isString: true,
                exists: true,
                escape: true,
            },
        };
    }

    /**
     * Returns a schema to validate the input of a GET /updates/firmware request.
     */
    static validateDownloadFirmware() {
        return {
            device_id: {
                in: ['query'],
                errorMessage: '<No device_id or not parseable to string>',
                isString: true,
                exists: true,
                escape: true,
            },
            new_firmware_url: {
                in: ['query'],
                errorMessage: '<No new_firmware_url or not parseable to Url>',
                isURL: true,
                exists: true,
                // escape: true,
            },
            // new_firmware_filename: {
            //     in: ['query'],
            //     errorMessage: '<No new_firmware_filename or not parseable to string>',
            //     isString: true,
            //     exists: true,
            //     escape: true,
            // },
        };
    }

    /**
     * Returns a schema to validate the input of a GET /updates/config request.
     */
    static validateDownloadConfig() {
        return {
            device_id: {
                in: ['query'],
                errorMessage: '<No device_id or not parseable to string>',
                isString: true,
                exists: true,
                escape: true,
            },
            new_config_url: {
                in: ['query'],
                errorMessage: '<No new_config_url or not parseable to Url>',
                isURL: true,
                exists: true,
                // escape: true,
            },
        };
    }

    // ######## VALIDATION FUNCTIONS ########
    /**
     * Check if the provided deviceId actually exists.
     *
     * @param {object} req
     * @param {object} res
     * @param {object} next
     */
    static async checkIfDeviceExists(req, res, next) {
        const responder = new Responder();
        const dbHandler = new DbHandler();

        // extract device_id either from the body or from the query
        const deviceId = req.body.device_id ? req.body.device_id : req.query.device_id;

        try {
            // query condition
            const condition = {
                where: { id: deviceId },
            };

            // check if the deviceId exists in the `device` table
            const device = await dbHandler.findOne('device', condition);

            // throw an error if the device does not exist
            if (!device) {
                logger.warn(`No device has been found for provided Id: ${deviceId}`);

                const deviceNotExistingError = {
                    msgCode: '<DeviceNotExists>',
                    err: `The device ${deviceId} does not exist`,
                    status: 422,
                };

                throw deviceNotExistingError;
            }
        } catch (error) {
            // send error response
            return responder.falseAndSend(res, error, error.status);
        }

        logger.info(`The device Id ${deviceId} exists`);

        // if validation was successful
        return next();
    }

    /**
     * This function checks if the amount of recfiles is within certain boundaries.
     *
     * Furthermore, it checks if for each recfile a checkum is provided by usage of
     * the following pattern: recfile <> sha, recfile2 <> sha2, recfile3 <> sha3, etc.
     *
     * If a recfile has a provided checksum, it is assigned to req.files.checksumSha1.
     *
     * Important:
     *
     * If either the amount of recfiles is not within boundaries (at least 1 and
     * maximum config.maxAmountRecfilesPerRequest) or the recfile names to not fit
     * the above pattern, the request won't be accepted.
     *
     * Furthermore, the request won't be accepted, if there is not a checksum for each recfile.
     *
     * @param {object} req
     * @param {object} res
     * @param {object} next
     */
    static validateRecfiles(req, res, next) {
        const responder = new Responder();
        // extract device_id
        const deviceId = req.body.device_id;

        try {
            // check if the amount of files is within the expected boundaries
            validateAmountOfFiles(req.files, deviceId);
            logger.info(`Amount of files within the specified boundaries for device ${deviceId}`);

            // iterate over all recfiles and check if for each recfile, a checksum is provided with the following pattern:
            // recfile <> sha, recfile2 <> sha2, recfile3 <> sha3, etc.
            _.forOwn(req.files, (file, paramName) => {
                // if recfile names do not match the pattern
                const recfileNamingError = {
                    msgCode: '<RecfileNamingError>',
                    err: `Recfile for device ${deviceId} does not match regex recfileX (while X is a number): ${paramName}`,
                    status: 422,
                };

                // check if each recfile has the name 'recfileX' while X is a number. This is needed to assign
                // each recfile to its checksum.
                const regex = /recfile\d+/;
                // Note, that the first recfile is named simply 'recfile' for compatibility purposes
                if (paramName === 'recfile' || regex.test(paramName)) {
                    // Firstly, separate each recfileX in 'recfile' and 'X' and then get the appropriate shaX:
                    // the first recfile does not have a number so 1 is assigned manually. For the other recfiles,
                    // the number is simply extracted by splitting e.g. recfile2 to '' and '2' (2 --> index 1)
                    const recFileNumber = (paramName === 'recfile') ? 1 : +paramName.split('recfile')[1];

                    // check if the recFileNumber is a number
                    if (isNaN(recFileNumber)) {
                        throw recfileNamingError;
                    }

                    // get all checksums
                    const checksums = getChecksums(req.body);

                    // assign the appropriate sha to its recfile
                    assignChecksumToRecfile(checksums, file, paramName, recFileNumber, deviceId);
                } else {
                    // Note: A recfileNamingError is thrown also if only one recfile does not fit the naming pattern
                    throw recfileNamingError;
                }
            });
        } catch (error) {
            // send error response
            return responder.falseAndSend(res, error, error.status);
        }

        logger.info(`Recfiles and checksums match needed patterns for device ${deviceId}`);

        // if validation was successful
        return next();
    }

    /**
     * Validate the recfiles against their checksums.
     *
     * @param {object} req
     * @param {object} res
     * @param {object} next
     */
    static validateRecfileWithChecksum(req, res, next) {
        const responder = new Responder();
        // extract device_id
        const deviceId = req.body.device_id;
        // extract files
        const { files } = req;

        try {
            // iterate over all recfiles and validate it against its checksum.
            _.forOwn(files, (file) => {
                // if the provided checksum does not equal the calculated one
                const checksumError = {
                    msgCode: '<RecfileWrongChecksum>',
                    err: `The checksum of the recfile ${file.name} for device ${deviceId} does not match the calculated one`,
                    status: 422,
                };

                // calculate the sha1 checksum of that file
                const calculatedSha1 = sha1(file.data);

                // check if the calculated checksum matches the provided checksum
                if (calculatedSha1 !== file.checksumSha1) {
                    // if not, throw an exception
                    throw checksumError;
                }
            });
        } catch (error) {
            // for testing purposes, all failed recfiles are moved to the recfile error queue
            moveRecfilesToErrorQueue(files, deviceId);

            // send error response
            return responder.falseAndSend(res, error, error.status);
        }

        logger.info(`The checksums of all recfiles are valid for device ${deviceId}`);

        // if validation was successful
        return next();
    }
};

// ######## HELPER FUNCTIONS ########
/**
 * Check if the amount of files is within the expected boundaries. If not, throw an exception.
 *
 * @param {object} files the recfiles
 * @param {string} deviceId the Id of the device that uploads recfiles
 */
function validateAmountOfFiles(files, deviceId) {
    const amountOfFiles = _.size(files);

    // check if there are files but not too many files
    if (!files) {
        // if no files are provided
        const noFilesError = {
            msgCode: '<NoRecfiles>',
            err: `No recfiles are provided for device ${deviceId}`,
            status: 422,
        };

        throw noFilesError;
    } else if (amountOfFiles > config.maxAmountRecfilesPerRequest) {
        // if there are more files than pointed out in the global config file
        const tooManyFilesError = {
            msgCode: '<TooManyRecfiles>',
            err: `Too many recfiles are provided for device ${deviceId} (amount: ${amountOfFiles})`,
            status: 413, // Payload Too Large
        };

        throw tooManyFilesError;
    }
}

/**
 * Extract all checksums out of the body that match the pattern 'shaX' while X is a number.
 *
 * @param {object} body
 *
 * @returns {array} the checksums in the form { X: shaX }
 */
function getChecksums(body) {
    const checksums = [];

    // iterate over the body and look for checksums with the form 'shaX' while X is a number.
    _.forOwn(body, (value, key) => {
        // check if each sha has the name 'shaX' while X is a number. This is needed to assign
        // each sha to its recfile.
        const regex = /sha\d+/;

        // Note, that the first sha is named simply 'sha' for compatibility purposes
        if (key === 'sha' || regex.test(key)) {
            // the first sha does not have a number so 1 is assigned manually. For the other checksums,
            // the number is simply extracted by splitting e.g. sha2 to '' and '2' (2 --> index 1)
            const shaNumber = (key === 'sha') ? 1 : +key.split('sha')[1];

            // check if the shaNumber is a number
            if (!isNaN(shaNumber)) {
                // add that checksum to the checksums array
                checksums.push({ shaNumber, sha: value });
            }
        }
    });

    return checksums;
}

/**
 * Assign a checksum to a recfile. Therefore, the recfileNumber is connected
 * to the checksum in the array that has the same value as shaNumber.
 *
 * @see getChecksums()
 *
 * @param {array} checksums the array containing all checkums
 * @param {object} recfile a recfile
 * @param {string} recfileName the name of the recfile
 * @param {number} recfileNumber the number of that recfile used for assign to correct checksum
 * @param {string} deviceId the Id of the device that uploads recfiles
 */
function assignChecksumToRecfile(checksums, recfile, recfileName, recfileNumber, deviceId) {
    // iterate over all the checksums and assign the appropriate sha to its file
    for (let i = 0; i < checksums.length; i++) {
        // assign recfileX to shaX
        if (checksums[i].shaNumber === recfileNumber) {
            recfile.checksumSha1 = checksums[i].sha;
            return;
        }
    }

    // if no checksum exists for a recfile
    const noChecksumForRecfileError = {
        msgCode: '<MissingChecksum>',
        err: `No checksum is provided for the recfile ${recfileName} uploaded by device ${deviceId}`,
        status: 422,
    };

    throw noChecksumForRecfileError;
}

/**
 * Function just for test purposes.
 *
 * Moves all wrong recfiles to the recfile error queue.
 *
 * @param {object} recFiles all recfiles to be moved
 * @param {string} deviceId the Id of the device that uploaded the recfiles
 */
function moveRecfilesToErrorQueue(recFiles, deviceId) {
    // move recfiles to recfile queue
    _.forOwn(recFiles, (file) => {
        // add the uuid before the file name and before the file extension like:
        // -- >recfilenameXYZ (UUID).rec
        file.name = `${(file.name).substring(0, (file.name).lastIndexOf('.'))} (${uuidv1()})${
            (file.name).substring((file.name).lastIndexOf('.'))}`;

        // create the recfile in the recfile queue folder
        const wstream = fs.createWriteStream(`${config.recfileErrorQueueFolder}/${file.name}`);

        // when the writing is finished
        wstream.on('finish', () => {
            logger.info(`Recfile '${file.name}' for device ${deviceId} has been moved to error queue`);
        });

        // error handler
        wstream.on('error', (error) => {
            logger.warn(`An error occurred moving recfile '${file.name}' to error queue for device ${deviceId}: ${error}`);
        });

        // write recfile buffer to file
        wstream.write(file.data);
        wstream.end();
    });
}
