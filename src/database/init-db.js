// ####### Running this script creates dummy vaules in the database and should only be used in a test or dev environment #######

// custom modules
const DbHandler = require('../logic/DbHandler');
const config = require('../../config');

// only create dummies in test or dev environment
if (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'test') {
    try {
        setupDevDb();
    } catch (error) {
        console.error(error);
    }
}

/**
 * Fill in some dummy values in the dev database.
 */
async function setupDevDb() {
    // handler to write into the test db specified in the config file
    const dbHandler = new DbHandler();

    // the device that should be found or created
    const device = {
        where: {
            id: '1',
        },
        defaults: { // set the default properties if it doesn't exist
            id: '1', device_type: 'obd', last_firmware_update: new Date(), last_config_update: new Date(),
        },
    };
    // create the device with id '1'
    await dbHandler.findOrCreate('device', device);

    // the user_vehicle that should be found or created
    const userVehicle = {
        where: {
            id: 4,
        },
        defaults: { // set the default properties if it doesn't exist
            id: 4, user_id: 15,
        },
    };
    // create user_vehicle with id 1
    await dbHandler.findOrCreate('user_vehicle', userVehicle);

    // add an entry to table 'user_vehicle_device' with an authentication token so that
    // the upcoming tests are able to authenticate themselves.
    const userVehicleDevice = {
        where: {
            user_vehicle_id: 4,
        },
        defaults: { // set the default properties if it doesn't exist
            user_vehicle_id: 4, device_id: '1', token: 'abcdefg', start_time: new Date(), stop_time: new Date(),
        },
    };
    await dbHandler.findOrCreate('user_vehicle_device', userVehicleDevice);

    // create an entry for the default firmware version specified in the config file. This is important
    // because all newly created device need a certain configuration to start with.
    const firmwareVersion = {
        where: {
            id: config.defaultFirmwareVersionId,
        },
        defaults: { // set the default properties if it doesn't exist
            id: config.defaultFirmwareVersionId,
        },
    };
    await dbHandler.findOrCreate('obd_firmware_version', firmwareVersion);
}

// export this function to be used within the test suite
exports.setupDevDb = setupDevDb;
