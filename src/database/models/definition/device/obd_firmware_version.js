// custom dependencies
const dbSchemata = require('../../../../../config').database.params.schema;

// the schema the table belongs to
const tableSchema = dbSchemata.device;

/**
 * @typedef {Object} obd_firmware_version
 *
 * @property {number} id the ID of the firmware version
 * @property {string} name the name of the firmware version
 * @property {string} filename the name of the firmware file
 * @property {string} url the url to the firmware file to be used for the update
 * @property {string} check_sum the check sum of the firmware file
 *
 * @return {obd_firmware_version}
 */
module.exports = (sequelize, DataTypes) => sequelize.define('obd_firmware_version', {
    id: {
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    filename: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    url: {
        type: DataTypes.STRING,
        allowNull: true,
    },
    check_sum: {
        type: DataTypes.STRING,
        allowNull: true,
    },
}, {
    schema: tableSchema,
    tableName: 'obd_firmware_version',
    // if timestamps are on true, sequelize is looking for columns named 'createdAt' and 'updatedAt'
    timestamps: false,
    // tells sequelize that table names are not camelcase but snakecase
    underscored: true,
});
